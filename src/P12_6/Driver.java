package P12_6;

/**
 * Tests TeachMath program
 * Created by Eryka on 11/6/2015.
 */
public class Driver {
    public static void main(String[] args) {
        TeachMath learn = new TeachMath();
        learn.start();
    }
}
