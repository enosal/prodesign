package P12_6;

/**
 * Problem Three Problems
 * Subtract any two one-digit integers whose subtraction is non-negative
 * Created by Eryka on 11/6/2015.
 */
public class ProblemL3 extends Problem {


    public ProblemL3() {
        super();
        makeRandomProb();
    }

    /*
    Construct the L2 problem
    */
    public void makeRandomProb() {
        int1 = rand.nextInt(10);
        if (int1 == 0)
            int2 = 0;
        else
            int2 = rand.nextInt(int1);
        ans = int1 - int2;
    }

    /*
    Displays L3 problems
     */
    @Override
    public void display() {
        System.out.printf("What is %d - %d? ", getInt1(), getInt2() );
    }



}