package P12_6;

/**
 * Problem One Problems
 * Add any two one-digit integers whose sum is less than 10
 * Created by Eryka on 11/6/2015.
 */
public class ProblemL1 extends Problem {

    public ProblemL1() {
        super();
        makeRandomProb();
    }

    /*
    Construct the L1 problem
    */
    public void makeRandomProb() {
        int1 = rand.nextInt(10);
        int2 = rand.nextInt(10-int1);
        ans = int1 + int2;
    }


}

