package P12_6;

import java.util.Random;

/**
 * Creates objects of Problem that have a first int, second int, answer
 * Created by Eryka on 11/6/2015.
 */
public class Problem {
    protected int int1;
    protected int int2;
    protected int ans;
    protected Random rand;

    /*
    Creates random number generator
     */
    public Problem() {
        rand = new Random();
    }

    /*
    Gets first integer
     */
    public int getInt1(){
        return int1;
    }

    /*
    Gets second integer
     */
    public int getInt2() {
        return int2;
    }

    /*
    Gets answer
     */
    public int getAnswer() {
        return ans;
    }

    /*
    Displays prob
     */
    public void display() {
        System.out.printf("What is %d + %d? ", getInt1(), getInt2() );
    }

    /*
    makeRandomProb for override
     */
    public void makeRandomProb(int currentLevel) {}

}
