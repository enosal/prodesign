package P12_6;

import java.util.NoSuchElementException;
import java.util.Scanner;

/**
 * A program to teach children math
 * Created by Eryka on 11/6/2015.
 */
public class TeachMath {
    private int tries;
    private int score;
    private int currentLevel;
    private Problem currentProb;
    private int childAns;
    private Scanner in;
    public final static int POINTS_TO_NEXT_LEVEL = 5;

    /*
    Constructs a program to teach children math
     */
    public TeachMath() {
        currentLevel = 0;
        resetLevel();
        in = new Scanner(System.in);
    }

    /*
    Resets score and tries
     */
    public void resetTries() {
        tries = 2;
    }

    /*
    Resets Level (tries and score)
     */
    public void resetLevel(){
        score = 0;
        currentLevel++;
        resetTries();
    }

    /*
    Starts the Math-teaching program;
     */
    public void start() {
        System.out.println("Welcome to TeachMath! Type in your answers as numbers. Type 'Quit' to stop playing");
        System.out.println("-------------------\n");

        //first problem
        currentProb = getProblem(currentLevel);
        currentProb.display();
        while (true) {
            try {
                in.hasNextInt();
                childAns = in.nextInt();
                checkAns(childAns, currentProb);
            }
            catch (NoSuchElementException e) {
                System.out.println("\n-------------------");
                System.out.println("Thanks for playing!");
                System.exit(0);
            }
        }
    }


    /*
    Gets a problem based on level
     */
    public Problem getProblem(int level) {
        Problem newProb;
        if (level == 1) {
            newProb = new ProblemL1();
        }
        else if (level == 2) {
            newProb = new ProblemL2();
        }
        else if (level == 3) {
            newProb = new ProblemL3();
        }
        else {
            //This should never be entered
            newProb = new Problem();
        }
        return newProb;
    }


    /*
    Compares a student's answer to the problem's answer
     */
    public boolean isCorrect(int givenAns, Problem prob) {
        return givenAns == prob.getAnswer();
    }

    /*
    Checks answer for a problem
     */
    public void checkAns(int givenAns, Problem prob) {
        if (isCorrect(givenAns, prob)) {
            score++;
            System.out.println("You're correct! :)");
            resetTries();
            checkPoints();
            currentProb = getProblem(currentLevel);
            currentProb.display();
        }
        else {
            tries--;
            if (tries > 0) {
                System.out.println("That's not right. Why don't you try again?");
                prob.display();
            } else {
                System.out.println("Let's try a different problem");
                resetTries();
                currentProb = getProblem(currentLevel);
                currentProb.display();
            }
        }
    }


    /*
    Checks points
     */
    public void checkPoints() {
        if (score == POINTS_TO_NEXT_LEVEL) {
            System.out.println("Congratulations! You have completed Level " + currentLevel + "!");
            resetLevel();
            if (currentLevel == 4) {
                System.out.println("\n-------------------");
                System.out.println("You have completed the game!\nThanks for playing!");
                System.exit(0);
            }
        }
    }


}
