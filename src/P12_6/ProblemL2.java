package P12_6;

/**
 * Problem Two Problems
 * Add any two one-digit integers
 * Created by Eryka on 11/6/2015.
 */
public class ProblemL2 extends Problem {

    public ProblemL2() {
        super();
        makeRandomProb();
    }

    /*
    Construct the L2 problem
    */
    public void makeRandomProb() {
        int1 = rand.nextInt(10);
        int2 = rand.nextInt(10);
        ans = int1 + int2;
    }

}
