package P12_8;

import java.util.Scanner;

/**
 * Tests VendingMachine
 * Created by Eryka on 11/6/2015.
 */
public class Driver {
    public static void main(String[] args) {
        VendingMachine myVM = new VendingMachine();
        Product proChips = new Product("Sour Cream and Onion Chips", 1.25);
        Product proMMs = new Product("Peanut Butter M&Ms", 1);
        Product proCookies = new Product("Chocolate Chip Cookies", 1.40 );
        myVM.addProduct(proMMs);
        myVM.addProduct(proMMs);
        myVM.addProduct(proMMs);
        myVM.addProduct(proCookies);
        myVM.addProduct(proChips);
        VMReader vmReader = new VMReader(myVM);
        vmReader.start();
    }

}
