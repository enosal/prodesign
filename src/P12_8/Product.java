package P12_8;

/**
 * Has inventory and information about a product.
 * Created by Eryka on 11/6/2015.
 */
public class Product {
    private String proName;
    private double proPrice;
    private int quantity;

    /*
        Create product
     */
    public Product(String name, double price) {
        proName = name;
        proPrice = price;
        quantity = 0;
    }

    /*
        Gets name
     */
    public String getName() {
        return proName;
    }

    /*
        Gets price
     */
    public double getPrice() {
        return proPrice;
    }

    /*
        Restock product by one
     */
    public void addProduct() {
        quantity += 1;
    }

    /*
        Remove product
     */
    public void removeProduct() {
        if (quantity > 0)
            quantity -= 1;
    }

    /*
        Gets quantity of product
     */
    public int getQuantity() {
        return quantity;
    }

    /*
        Checks if product is sold out
     */
    public boolean isSoldOut() {
        return quantity == 0;

    }
}
