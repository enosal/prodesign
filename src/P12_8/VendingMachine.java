package P12_8;

import java.util.ArrayList;

/**
 * Simulates a vending machine that takes coins and outputs products (if the cost is enough)
 * Created by Eryka on 11/6/2015.
 */
public class VendingMachine {
    private ArrayList<Product> inventory;
    private double balance;

    public VendingMachine() {
        inventory = new ArrayList<>();
        balance = 0;
    }

    /*
        Adds a product
     */
    public void addProduct(Product pro) {
        pro.addProduct();
        if (!inventory.contains(pro)) {
            inventory.add(pro);
        }
    }

    /*
        Displays inventory of products
     */
    public void displayInventory(){
        System.out.println("\nDISPLAYING PRODUCTS");
        for (int i = 0; i < inventory.size(); i++ ) {
            System.out.printf("%d) %s - %d%n", i+1, inventory.get(i).getName(), inventory.get(i).getQuantity());
        }
    }

    /*
    Displays products
 */
    public void displayProducts(){
        System.out.println("\nDISPLAYING PRODUCTS");
        for (int i = 0; i < inventory.size(); i++ ) {
            System.out.printf("%d) %s - %.2f%n", i+1, inventory.get(i).getName(), inventory.get(i).getPrice());
        }
    }

    /*
        Gets a product from the user input (off by one)
     */
    public Product getProduct(int i) {
        return inventory.get(i-1);
    }

    /*
        Gives number of unique products
     */
    public int numProducts() {
        return inventory.size();
    }

    /*
        Add coins to balance
     */
    public void addCoin(double coinAmt) {
        balance += coinAmt;
        System.out.printf("%nADDED $%.2f%n", coinAmt);
    }

    /*
        Displays current balance on vending machine
     */
    public void displayBalance() {
        System.out.printf("\nBALANCE IS $%.2f%n", balance);
    }

    /*
        Clear balance
    */
    public void clearBalance(){
        balance = 0;
    }

     /*
        Press to buy a product
     */
    public void pressBuy(Product pro) {
        if (balance >= pro.getPrice()) {
            if (pro.isSoldOut()) {
                clearBalance();
                System.out.println("\nSOLD OUT. Your money was returned.");
            }
            else {
                purchase(pro);
                disperse(pro);
            }
        }
        else {
            clearBalance();
            System.out.println("\nINSUFFICIENT FUNDS. Your money was returned");
        }
    }

    /*
        Purchases a product
    */
    public void purchase(Product pro) {
        balance -= pro.getPrice();
    }

    /*
        Disperses product
     */
    public Product disperse(Product pro) {
        pro.removeProduct();
        System.out.printf("\nBOUGHT %s FOR $%.2f%n", pro.getName().toUpperCase(), pro.getPrice());
        return pro;

    }

}
