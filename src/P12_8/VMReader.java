package P12_8;

import java.util.InputMismatchException;
import java.util.NoSuchElementException;
import java.util.Scanner;

/**
 * Reads in input for a VendingMachine
 * Created by Eryka on 11/6/2015.
 */
public class VMReader {
    Scanner in = new Scanner(System.in);
    private boolean buyingFromMachine = true;
    private boolean coinCtrl;
    private boolean buyCtrl;
    private boolean operCtrl;
    private boolean restockCtrl;
    private int input;
    private VendingMachine vm;

    /*
        Creates vending machine
     */
    public VMReader(VendingMachine vm) {
        this.vm = vm;
    }

    /*
        Main loop that prompts for input
     */
    public void start() {
        System.out.println("INSTRUCTIONS: Please enter only integer numbers. User action required when '>>' appears");
        while (buyingFromMachine) {
            System.out.println("\n----------------------------");
            System.out.print(">>MAIN MENU. 1) Display Products\t 2)Add Coins\t 3)Display Balance\t 4)Operator Options\t 0)Quit:\t");
            try {
                in.hasNextInt();
                input = in.nextInt();
                if (input == 1) {
                    buyCtrl = true;
                    vm.displayProducts();
                    buyLoop();
                }
                else if (input == 2) {
                    coinCtrl = true;
                    coinLoop();
                }
                else if (input == 3) {
                    vm.displayBalance();
                }
                else if (input == 4) {
                    operCtrl = true;
                    operLoop();
                }
                else if (input == 0) {
                    buyingFromMachine = false;
                }
                else {
                    System.out.println("\nWARNING: Please enter a valid option");
                }
            }
            catch (NoSuchElementException e) {
                System.out.println("WARNING: Improper or no input given");
                break;
            }
        }
    }

    /*
        Loops to buy products
     */
    public void buyLoop() {
        while (buyCtrl) {
            System.out.print("\n>>Buy product by number or 0)Main Menu:\t");
            try {
                in.hasNextInt();
                input = in.nextInt();
                if (input == 0) {
                    buyCtrl = false;
                    break;
                }
                else {
                    if (input <= vm.numProducts()) {
                        buyCtrl = false;
                        vm.pressBuy(vm.getProduct(input));
                    }
                }


            } catch (NoSuchElementException e) {
                System.out.println("WARNING: Improper or no input given");
                break;
            }
        }
    }

    /*
        Loops for valid coin input
     */
    public void coinLoop() {
        while (coinCtrl) {
            System.out.print("\n>>Input coin by number. 1)Penny\t 2)Nickle\t 3)Dime\t 4)Quarter\t 5)Half-Dollar\t 6)Dollar\t 0)Main Menu:\t");
            try {
                in.hasNextInt();
                switch (in.nextInt()) {
                    case 1:
                        vm.addCoin(Coin.PENNY_VALUE);
                        coinCtrl = false;
                        break;
                    case 2:
                        vm.addCoin(Coin.NICKLE_VALUE);
                        coinCtrl = false;
                        break;
                    case 3:
                        vm.addCoin(Coin.DIME_VALUE);
                        coinCtrl = false;
                        break;
                    case 4:
                        vm.addCoin(Coin.QUARTER_VALUE);
                        coinCtrl = false;
                        break;
                    case 5:
                        vm.addCoin(Coin.HALF_VALUE);
                        coinCtrl = false;
                        break;
                    case 6:
                        vm.addCoin(Coin.DOLLAR_VALUE);
                        coinCtrl = false;
                        break;
                    case 0:
                        coinCtrl = false;
                        break;
                    default:
                        break;
                }
            } catch (NoSuchElementException e) {
                System.out.println("WARNING: Improper or no input given");
                break;
            }
        }
    }

    /*
        Loops for operator options
     */
    public void operLoop() {
        while (operCtrl) {
            System.out.print("\n>>OPERATOR OPTIONS. 1)Restock\t 2)Clear Balance\t 0)Main Menu:\t");
            try {
                in.hasNextInt();
                switch (in.nextInt()) {
                    case 1:
                        vm.displayInventory();
                        restockCtrl = true;
                        restockLoop();
                        operCtrl = false;
                        break;
                    case 2:
                        vm.clearBalance();
                        System.out.println("\nBALANCE CLEARED");
                        operCtrl = false;
                        break;
                    case 0:
                        operCtrl = false;
                        break;
                    default:
                        break;
                }
            } catch (NoSuchElementException e) {
                System.out.println("WARNING: Improper or no input given");
                break;
            }
        }
    }

    /*
        Loops for restock input
     */
    public void restockLoop() {
        while (restockCtrl) {
            System.out.print("\n>>Restock product by number or 0)Main Menu:\t");
            try {
                in.hasNextInt();
                input = in.nextInt();
                if (input == 0) {
                    restockCtrl = false;
                    break;
                }
                else {
                    if (input <= vm.numProducts()) {
                        System.out.println("\n" + vm.getProduct(input).getName().toUpperCase() + " ADDED");
                        restockCtrl = false;
                        vm.addProduct(vm.getProduct(input));
                    }
                }


            } catch (NoSuchElementException e) {
                System.out.println("WARNING: Improper or no input given");
                break;
            }
        }
    }


}
