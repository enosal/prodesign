package P12_8;

/**
 * Contains constants for coin values
 * Created by Eryka on 11/6/2015.
 */
public final class Coin {

    public static final double PENNY_VALUE = 0.01;
    public static final double NICKLE_VALUE = 0.05;
    public static final double DIME_VALUE = 0.1;
    public static final double QUARTER_VALUE = 0.25;
    public static final double HALF_VALUE = 0.50;
    public static final double DOLLAR_VALUE = 1.00;
}
